;; -*- mode: Lisp; Syntax: Common-Lisp; Package: cello; -*-
;;;
;;; Copyright � 2004 by Kenneth William Tilton.
;;;
;;; Permission is hereby granted, free of charge, to any person obtaining a copy 
;;; of this software and associated documentation files (the "Software"), to deal 
;;; in the Software without restriction, including without limitation the rights 
;;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
;;; copies of the Software, and to permit persons to whom the Software is furnished 
;;; to do so, subject to the following conditions:
;;;
;;; The above copyright notice and this permission notice shall be included in 
;;; all copies or substantial portions of the Software.
;;;
;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
;;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
;;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
;;; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
;;; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
;;; FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS 
;;; IN THE SOFTWARE.

;;; $Header: /project/cello/cvsroot/cl-ftgl/cl-ftgl.lisp,v 1.1 2005/05/31 14:28:59 ktilton Exp $

(defpackage #:cl-ftgl
  (:nicknames #:ftgl)
  (:use #:common-lisp #:ffx #:ogl)
  (:export #:ftgl 
    #:ftgl-pixmap 
    #:ftgl-texture 
    #:ftgl-bitmap
    #:ftgl-polygon 
    #:ftgl-extruded 
    #:ftgl-outline
    #:ftgl-string-length 
    #:ftgl-get-ascender 
    #:ftgl-get-descender
    #:ftgl-make 
    #:cl-ftgl-init 
    #:cl-ftgl-reset 
    #:xftgl 
    #:ftgl-render
    #:ftgl-font-ensure
    #:ftgl-ensure-ifont
    #:cl-ftgl-set-home-dir
    #:cl-ftgl-get-home-dir
    #:cl-ftgl-set-dll-filename
    #:cl-ftgl-get-dll-filename
    #:cl-ftgl-set-supporting-dll-filename
    #:cl-ftgl-get-supporting-dll-filename
    #:cl-ftgl-set-freetype-dll-filename
    #:cl-ftgl-get-freetype-dll-filename
    #:*ftgl-dynamic-lib-path*
    #:*font-directory-path*
    #:*gui-style-default-face*
    #:*gui-style-button-face*))

(in-package :cl-ftgl)

;; ----------------------------------------------------------------------------
;; EXTERNAL DEPENDENCIES
;; ----------------------------------------------------------------------------
;; 
;; ENVIRONMENT VARIABLES:
;; 
;;   FTGL_HOME             Denotes the FTGL root installation directory
;;   FTGL_SHLIB            Gives the name of the FTGL shared lib/DLL. If given
;;                         as a relative path then the full path to the 
;;                         shared lib/DLL will be constructed as
;;                         $FTGL_HOME/lib/$FTGL_SHLIB. If it cannot be loaded
;;                         from that path the $LD_LIBRARY_PATH or $SHLIB_PATH
;;                         will be tried next (using the system's dlopen/
;;                         shlload functions).
;;
;; ----------------------------------------------------------------------------
;; ----------------------------------------------------------------------------

(in-package :cl-ftgl)

(defparameter *ftgl-dll*  nil)
(defparameter *ftgl-supporting-dll* nil)
(defparameter *ftgl-freetype-dll* nil)

(defparameter *cl-ftgl-home-dir* nil) ; cl-ftgl home/install directory
(defparameter *cl-ftgl-dll-filename* nil) ; FTGL.DLL / libFTGL.so filename
     ; Absolute on Windows, relative
     ; on Unix/Linux
(defparameter *cl-ftgl-dll-full-filename* nil) ; Constructed filename

(defparameter *cl-ftgl-supporting-dll-filename* nil) ; NIL on Windows (!)
     ; libstdc++.so on Linux
(defparameter *cl-ftgl-supporting-dll-full-filename* nil) ; Constructed filename

(defparameter *cl-ftgl-freetype-dll-filename* nil) ; NIL on Windows (!)
     ; libfreetype.so on Linux
(defparameter *cl-ftgl-freetype-dll-full-filename* nil) ; Constructed filename

(defparameter *ftgl-fonts-loaded*    nil)
(defvar       *test-fonts*)
(defparameter  g_rot                 0)

;; ----------------------------------------------------------------------------
;; FOREIGN FUNCTION INTERFACE
;; ----------------------------------------------------------------------------


;; (defun-ffx :unsigned-byte "ftgl" "fgcSetFaceSize" (:void *f :int size :int res))

(PROGN (UFFI:DEF-FUNCTION ("fgcSetFaceSize" FGCSETFACESIZE)
                          ((*F (* :VOID)) (SIZE :INT) (RES :INT)) :RETURNING :UNSIGNED-BYTE
                          :MODULE "ftgl")
       (DEFUN FGC-SET-FACE-SIZE (*F SIZE RES)
         (LET ( (c-SIZE (COERCE SIZE 'INTEGER)) (c-RES (COERCE RES 'INTEGER)))
           (print (list `(FGC-SET-FACE-SIZE ,*f ,c-size ,c-res)))
           (PROG1 (FGCSETFACESIZE *F c-SIZE c-RES))))
       (EVAL-WHEN (COMPILE EVAL LOAD) (EXPORT '(FGCSETFACESIZE FGC-SET-FACE-SIZE))))

(defun-ffx :int "ftgl" "fgcCharTexture" (:void *f :int charCode))
(defun-ffx :float "ftgl" "fgcAscender" (:void *f))
(defun-ffx :float "ftgl" "fgcDescender" (:void *f))
(defun-ffx :float "ftgl" "fgcStringAdvance" (:void *f :cstring text))
(defun-ffx :float "ftgl" "fgcStringX" (:void *f :cstring text))
(defun-ffx :void "ftgl" "fgcRender" (:void *f :cstring text))
(defun-ffx :void "ftgl" "fgcBuildGlyphs" (:void *f))
(defun-ffx :void "ftgl" "fgcFree" (:void *f))

(defun-ffx (* :void) "ftgl" "fgcBitmapMake" (:cstring typeface))
(defun-ffx (* :void) "ftgl" "fgcPixmapMake" (:cstring typeface))
(defun-ffx (* :void) "ftgl" "fgcTextureMake" (:cstring typeface))
(defun-ffx (* :void) "ftgl" "fgcPolygonMake" (:cstring typeface))
(defun-ffx (* :void) "ftgl" "fgcOutlineMake" (:cstring typeface))
(defun-ffx (* :void) "ftgl" "fgcExtrudedMake" (:cstring typeface))
(defun-ffx :unsigned-byte "ftgl" "fgcSetFaceDepth" (:void * :float depth))

;; ----------------------------------------------------------------------------
;; FUNCTIONS/METHODS
;; ----------------------------------------------------------------------------

(defun cl-ftgl-reset ()
  (setq *ftgl-dll*             nil 
      *ftgl-fonts-loaded*    nil
      )
  )

(defun cl-ftgl-set-home-dir ( home-dir )
  (setq *cl-ftgl-home-dir* home-dir))

(defun cl-ftgl-get-home-dir ()
  (values *cl-ftgl-home-dir*))

(defun cl-ftgl-set-dll-filename ( filename )
  (setq *cl-ftgl-dll-filename* filename))

(defun cl-ftgl-get-dll-filename ()
  (values *cl-ftgl-dll-filename*))

(defun cl-ftgl-set-supporting-dll-filename ( filename )
  (setq *cl-ftgl-supporting-dll-filename* filename))

(defun cl-ftgl-get-supporting-dll-filename ()
  (values *cl-ftgl-supporting-dll-filename*))

(defun cl-ftgl-set-freetype-dll-filename ( filename )
  (setq *cl-ftgl-freetype-dll-filename* filename))

(defun cl-ftgl-get-freetype-dll-filename ()
  (values *cl-ftgl-freetype-dll-filename*))

#+test
(cl-ftgl-test 'cl-ftgl-test-disp-fc)

(defvar *start*)
(defvar *frames*)
(defun now () (/ (get-internal-real-time) internal-time-units-per-second))

(defun cl-ftgl-test (&optional (disp-fn 'cl-ftgl-test-disp-fc))
  (setf *start* (now)
    *frames* 0)
  (cl-ftgl-reset)
  (setq *test-fonts*
    (mapcar (lambda (mode)
       (cons mode (ftgl-make mode *gui-style-default-face* 48 96 18)))
     '(:texture :pixmap :bitmap :outline :polygon :extruded)))
  (ogl::lesson-14 disp-fn))

(defun test-font (mode)
  (cdr (assoc mode *test-fonts*)))

(ff-defun-callable :cdecl :void cl-ftgl-test-disp-fc ()
     (cl-ftgl-test-disp))


(defun cl-ftgl-test-disp ()
  (incf *frames*)
  (gl-load-identity)      ;; Reset The Current Modelview Matrix
  (gl-clear-color 0.0 0.0 0.0  0.5)
  (gl-clear (+ gl_color_buffer_bit gl_depth_buffer_bit))
  
  (gl-translatef 0.0f0 0.0f0 2.0f0)   ;; Move Into The Screen
  ;; Pulsing Colors Based On The Rotation
  (gl-color3f (* 1.0f0 (cos (/ g_rot 20.0f0)))
       (* 1.0f0 (sin (/ g_rot 25.0f0)))
       (- 1.0f0 (* 0.5f0 (cos (/ g_rot 17.0f0)))))

  (gl-scalef 0.006  0.006  0.0)
  (gl-disable gl_lighting)
  (gl-translatef -100 -200 0)
  (gl-enable gl_texture_2d)
  (ftgl-render (test-font :texture)
    (format nil "texture ~d" (floor (/ *frames*
                                      (max 1 (- (now) *start*))))))
  (gl-translatef 100 200 0)

  (gl-translatef -100 200 0)
  (gl-line-width  3)
  (ftgl-render (test-font :outline) "un-rotated outline")
  (gl-translatef 100 -200 0)

  (gl-translatef -200 100 0)
  (ftgl-render (test-font :polygon) "un-rotated polygon")
  (gl-translatef 200 -100 0)

  (with-matrix ()
    (gl-polygon-mode gl_front_and_back gl_line)
    (gl-rotatef g_rot 1.0f0 0.5f0 0.0f0)
    (gl-scalef 4 4 4)
    (gl-translatef -70 -20 0)
    (ftgl-render (test-font :extruded) "NeHe")
    (gl-polygon-mode gl_front_and_back gl_fill)
    )

  (gl-rotatef g_rot 1.0f0 0.0f0 0.0f0)   ;; Rotate On The X Axis
  (gl-rotatef (* g_rot 1.5f0) 0.0f0 1.0f0 0.0f0) ;; Rotate On The Y Axis
  (gl-rotatef (* g_rot 1.4f0) 0.0f0 0.0f0 1.0f0) ;; Rotate On The Z Axis

  (gl-push-matrix)
  
  (gl-enable gl_texture_2d)
  (gl-disable gl_lighting)
  (ftgl-render (test-font :texture) "NeHe 14 texture")
  
  (gl-raster-pos3i 10 10 0)
  (ftgl-render (test-font :pixmap) "NeHe 14 pixmap")

  (gl-raster-pos3i 10 -30 0)
  (ftgl-render (test-font :bitmap) "NeHe 14 bitmap")

     ;(gl-line-width 3)

  (gl-pop-matrix)
  
  (glut-swap-buffers)
  
  (incf g_rot 0.4f0)
  (glut-post-redisplay))

#+not
(defun cl-ftgl-init ()
  (setq *ftgl-dll* (uffi:load-foreign-library *cl-ftgl-dll-full-filename*
                     :force-load #+lispworks t 
                     #-lispworks nil
                     :module "ftgl")))

(defun cl-ftgl-init ()
  
  ;; LOCATE CONFIG FILE
  ;; First, search for the installation home directory
  
  ;; PORTING: frgo, 2004-03-06: get environment variable, getenv               
  
  (when *ftgl-dll* (return-from cl-ftgl-init))
  
  (unless *cl-ftgl-home-dir*
    (setq *cl-ftgl-home-dir* 
          #+allegro (sys:getenv "FTGL_HOME")
          #-allegro nil
          ))
  
  (unless (or *cl-ftgl-dll-filename*
            (setq *cl-ftgl-dll-filename*
                  (or *ftgl-dynamic-lib-path*
                    #+allegro (sys:getenv "FTGL_SHLIB")
                    #-allegro nil)))
    (break "FTGL dynamic library filename must be bound to *cl-ftgl-dll-filename*
~%in configure.lisp, or to environment variable FTGL_SHLIB"))
  
  
  
  ;; FIDDLE WITH PATHS
  
  (when (and *cl-ftgl-home-dir*
          (not (eq #\/ (char *cl-ftgl-home-dir*
                         (- (length *cl-ftgl-home-dir*) 1)))))
    (setq *cl-ftgl-home-dir* (concatenate 'string *cl-ftgl-home-dir* "/")))
  
  (print "CL-FTGL-INIT: *** CP 1 ***") 
  
  #+(or mswindows win32)
  (setq *cl-ftgl-dll-full-filename* *cl-ftgl-dll-filename*)
  #-(or mswindows win32)
  (progn
    (if (not (eq (char *cl-ftgl-dll-filename* 0) #\/))
        (setq *cl-ftgl-dll-full-filename* 
              (make-pathname (concatenate 'string 
                               *cl-ftgl-home-dir* 
                               "lib/"
                               *cl-ftgl-dll-filename*)))
      (setq *cl-ftgl-dll-full-filename* *cl-ftgl-dll-filename*)
      )
    
    (assert (eq (char *cl-ftgl-dll-full-filename* 0) #\/)
      () "CL-FTGL::CL-FTGL-INIT *** INVALID FTGL SHLIB FILENAME ~a !"
      *cl-ftgl-dll-full-filename* )
    
    (when *cl-ftgl-supporting-dll-filename*
      
      (if (not (eq (char *cl-ftgl-supporting-dll-filename* 0) #\/))
          (setq *cl-ftgl-supporting-dll-full-filename* 
                (make-pathname (concatenate 'string 
                                 *cl-ftgl-home-dir* 
                                 "lib/"
                                 *cl-ftgl-supporting-dll-filename*)))
        (setq *cl-ftgl-supporting-dll-full-filename* *cl-ftgl-supporting-dll-filename*)
        ))
    
    (when *cl-ftgl-freetype-dll-filename*
      
      (if (not (eq (char *cl-ftgl-freetype-dll-filename* 0) #\/))
          (setq *cl-ftgl-freetype-dll-full-filename* 
                (make-pathname (concatenate 'string 
                                 *cl-ftgl-home-dir* 
                                 "lib/"
                                 *cl-ftgl-freetype-dll-filename*)))
        (setq *cl-ftgl-freetype-dll-full-filename* *cl-ftgl-freetype-dll-filename*)
        )))
  
  ;; LOAD DLLS
  
  (when *cl-ftgl-supporting-dll-filename*
    (setq *ftgl-supporting-dll* 
          (uffi:load-foreign-library *cl-ftgl-supporting-dll-full-filename*
            :force-load #+lispworks t 
            #-lispworks nil
            :module "ftgl"))
    
    
    ;; COMMENT: frgo, 2004-03-06: SHOULD BE CHANGED FOR A PRODUCTION VERSION
    (assert (not (eq *ftgl-supporting-dll* nil))
      () "CL-FTGL::CL-FTGL-INIT *** FTGL SUPPORT SHLIB COULD NOT BE LOADED FROM ~a !"
      *cl-ftgl-supporting-dll-full-filename*))
  
  #-(or mswindows win32)
  (when *cl-ftgl-freetype-dll-filename*
    (setq *ftgl-freetype-dll* 
          (uffi:load-foreign-library *cl-ftgl-freetype-dll-full-filename*
            :force-load #+lispworls t 
            #-lispworks nil
            :module "ftgl"))
    
    
    ;; COMMENT: frgo, 2004-03-06: SHOULD BE CHANGED FOR A PRODUCTION VERSION
    (assert (not (eq *ftgl-freetype-dll* nil))
      () "CL-FTGL::CL-FTGL-INIT *** FTGL FREETYPE SHLIB COULD NOT BE LOADED FROM ~a !"
      *cl-ftgl-freetype-dll-full-filename*))
  
  (print "!!!! Loading FTGL")
  (setq *ftgl-dll* (uffi:load-foreign-library *cl-ftgl-dll-full-filename*
                     :force-load #+lispworks t 
                     #-lispworks nil
                     :module "ftgl"))
  
  ;; COMMENT: frgo, 2004-03-06: SHOULD BE CHANGED FOR A PRODUCTION VERSION
  (assert (not (eq *ftgl-dll* nil))
    () "CL-FTGL::CL-FTGL-INIT *** FTGL SHLIB COULD NOT BE LOADED FROM ~a !"
    *cl-ftgl-dll-full-filename*)
  )

(defun ftgl-font-ensure (type face size target-res &optional (depth 0))
  (let ((fspec (list type face size target-res depth)))
    (or (cdr (assoc fspec *ftgl-fonts-loaded* :test 'equal))
      (let ((f (apply 'ftgl-make fspec)))
        (push (cons fspec f) *ftgl-fonts-loaded*)
        f))))

(defun ftgl-make (type face size target-res &optional (depth 0))
  (print (list "ftgl-make entry" type face size))
  (funcall (ecase type
             (:bitmap 'make-ftgl-bitmap)
             (:pixmap 'make-ftgl-pixmap)
             (:texture 'make-ftgl-texture)
             (:outline 'make-ftgl-outline)
             (:polygon 'make-ftgl-polygon)
             (:extruded 'make-ftgl-extruded))
    :face face
    :size size
    :target-res target-res
    :depth depth))

;; --------- ftgl structure -----------------

(defstruct ftgl
  face size target-res depth
  descender ascender bboxes
  ifont)

(defstruct (ftgl-disp (:include ftgl))
  ready-p)

(defstruct (ftgl-pixmap (:include ftgl-disp)))
(defstruct (ftgl-texture (:include ftgl-disp)))
(defstruct (ftgl-bitmap (:include ftgl)))
(defstruct (ftgl-polygon (:include ftgl)))
(defstruct (ftgl-extruded (:include ftgl-disp)))
(defstruct (ftgl-outline (:include ftgl)))

(defmethod ftgl-ready (font)
  (declare (ignorable font))
  t)

(defmethod (setf ftgl-ready) (new-value (font ftgl-disp))
  (setf (ftgl-disp-ready-p font) new-value))

(defmethod (setf ftgl-ready) (new-value font)
  (declare (ignore new-value font)))

(defmethod ftgl-ready ((font ftgl-disp))
  (ftgl-disp-ready-p font))


#+allegro
(defun xftgl ()
  (dolist (dll (ff:list-all-foreign-libraries))
    (when (search "ftgl" (pathname-name dll))
      (print `(unloading foreign library ,dll))
      (ff:unload-foreign-library dll)
      (cl-ftgl-reset))))

(defun ftgl-get-ascender (font)
  (or (ftgl-ascender font)
    (setf (ftgl-ascender font)
        (fgc-ascender (ftgl-ensure-ifont font)))))

(defun ftgl-get-descender (font)
  (or (ftgl-descender font)
    (setf (ftgl-descender font)
        (fgc-descender (ftgl-ensure-ifont font)))))

(defun ftgl-ensure-ifont (font)
  (or (ftgl-ifont font)
    (setf (ftgl-ifont font) (ftgl-font-make font))))

(defun ftgl-font-make (font)
  ;;(print (list "ftgl-font-make entry" font))
  (let ((path (merge-pathnames
               (make-pathname :name (string (ftgl-face font)) :type "ttf")
               *font-directory-path*)))
    (if (probe-file path)
        (uffi:with-cstring (fpath (namestring path))
          (let ((f (fgc-font-make font fpath)))
            (if f
                (progn
                  ;;(ogl::dump-lists 1 10000)
                  (fgc-set-face-size f (ftgl-size font) (ftgl-target-res font))
                  f)
              (error "cannot load ~a font ~a" (type-of font) fpath))))
      (error "Font not found: ~a" path))))

(defun ftgl-render (font s)
  (uffi:with-cstring (cs s)
      (fgc-render (ftgl-ensure-ifont font) cs)))

(defmethod fgc-font-make :before (font fpath)
  (declare (ignore font fpath))
  (cl-ftgl-init))

(defmethod fgc-font-make ((font ftgl-pixmap) fpath)
  (fgc-pixmap-make fpath))
  
(defmethod fgc-font-make ((font ftgl-bitmap) fpath)
  (fgc-bitmap-make fpath))
  
(defmethod fgc-font-make ((font ftgl-texture) fpath)
  (fgc-texture-make fpath))

(defmethod fgc-font-make ((font ftgl-extruded) fpath)
  (let ((fgc (fgc-extruded-make fpath)))
    (fgc-set-face-depth fgc (ftgl-depth font))
    fgc))

(defmethod fgc-font-make ((font ftgl-outline) fpath)
  (fgc-outline-make fpath))

(defmethod fgc-font-make ((font ftgl-polygon) fpath)
  (fgc-polygon-make fpath))

(defun ftgl-string-length (font cs)
  (fgc-string-advance (ftgl-ensure-ifont font) cs))

(defmethod font-bearing-x ((font ftgl) &optional (text "m"))
  (uffi:with-cstring (cs text)
    (fgc-string-x (ftgl-ensure-ifont font) cs)))

(defmethod font-bearing-x (font &optional text)
  (declare (ignorable font text))
  0)

